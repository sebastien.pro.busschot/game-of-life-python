# -*- coding: utf-8 -*-
"""
Created on Fri Jan  8 08:51:04 2021

@author: seb
"""
from matplotlib.animation import FuncAnimation
import matplotlib.pyplot as plt
import numpy as np
import random

#fig, ax = plt.subplots()

#img = ax.imshow(Z, interpolation='None', cmap='viridis', aspect='equal')

#ani = animation.FuncAnimation(fig, update, fargs=(Z, img), interval=100)

#plt.show()


#def update(frame, Z, img):
#    rules(Z) #apliquer regles
#    img.set_array(Z)
#    return img,


class Engine:
    
    def __init__(self, sheme, size):
       self.sheme = sheme
       self.size = size if size > len(sheme) else len(sheme)
       self.getMatrix()
       self.window(10)
       
    def window(self, s):
        self.fig = plt.figure(figsize=(s, s))
        self.axes = plt.subplot(111)
        
        self.img = self.axes.imshow(self.matrix, interpolation='None', cmap='viridis', aspect='equal')
        self.anim = FuncAnimation(self.fig, self.update, fargs=(self.matrix, self.img), interval=100)
        plt.axis('off')
        plt.show()
       
    def update(self, frame, matrix, img):
        self.foreEachNode()
        img.set_array(matrix)
        return img,
    
    def getMatrix(self):
        self.matrix = np.zeros( (self.size, self.size) )
        
        maxX = len(self.sheme[0])
        maxY = len(self.sheme)
        startX = random.randrange(self.size-maxX)
        startY = random.randrange(self.size-maxY)
        
        for yIndex in range(maxY):
            for xIndex in range(maxX):
                self.matrix[yIndex+startY][xIndex+startX] = self.sheme[yIndex][xIndex]
        
        return self.matrix
    
    def printMatrix(self,matrix):
        for line in matrix:
            print(line)
            
    def foreEachNode(self):
        lenX = len(self.matrix)
        lenY = lenX
        
        for indexY in range(lenY):
            for indexX in range(lenX):
                nw = self.matrix[(indexY-1)%lenY][(indexX-1)%lenX]
                w  = self.matrix[indexY%lenY][(indexX-1)%lenX]
                sw = self.matrix[(indexY+1)%lenY][(indexX-1)%lenX]
                s  = self.matrix[(indexY+1)%lenY][indexX%lenX]
                se = self.matrix[(indexY+1)%lenY][(indexX+1)%lenX]
                e  = self.matrix[indexY%lenY][(indexX+1)%lenX]
                ne = self.matrix[(indexY-1)%lenY][(indexX+1)%lenX]
                n  = self.matrix[(indexY-1)%lenY][indexX%lenX]
                
                score = nw + w + sw + s + se + e + ne + n
                previus = int(self.matrix[indexY][indexX])
                
                if self.matrix[indexY][indexX] :
                    #print("\nNew state is 1" if score == 2 or score == 3 else "\nNew state is 0")
                    self.matrix[indexY][indexX] = score == 2 or score == 3
                else:
                    #print("\nNew state is 1" if score == 3 else "\nNew state is 0")
                    self.matrix[indexY][indexX] = score == 3
                
                #print("score : ",score," previus state : ",previus," new state : ",self.matrix[indexY][indexX])
                