# -*- coding: utf-8 -*-
import pickle
import re

class RleParser :
    
    def __init__(self, file):
        self.file = open(file, "r")
        matrixString = ""
        while True:
            line = self.file.readline()
            if(line == ""):
                break
            
            line = re.sub("\\n", "", line)
            isSize = re.search("^x", line)
            isMatrix = re.search(".*\$+.*", line)
            isLast = re.search(".*\$+.*!$", line)
        
            if isSize:
                self.getXY(line)
            elif isMatrix:
                if(self.x | self.y > 500):
                    break
                matrixString += line
                if(isLast):
                    gridLine = re.split("\$", re.sub("\s!", "", matrixString))
                    self.matrixLine(gridLine)
                    break
        
        pass
        self.file.close()
        
    def matrixLine(self,textLine):
        num = 1
        matrixLine = []
        matrixIndex = 0
        for line in textLine:
            matrixLine.append([])
            for char in line:
                try:
                    num = int(char)
                except:
                    for i in range(0,num):
                        matrixLine[matrixIndex].append(self.IO(char))
                    num = 1
                    
            lineLen = len(matrixLine[matrixIndex])
            if lineLen < self.x:
                value = 1 if matrixLine[matrixIndex][lineLen-1] == "b" else 0
                for i in range(lineLen,self.x):
                    matrixLine[matrixIndex].append(value)
            
            matrixIndex = matrixIndex+1
            
        self.matrix = matrixLine
        
    def IO(self, char):
         if(char == "b"):
            return 0
         else:
            return 1
        
    def printMatrix(self):
        for grid in self.matrix:
            print(grid)
            
    def getXY(self, line):
        splited = re.split(",", line)
        for case in splited:
            case = case.strip()
            if re.match("^x|^y", case):
                splited = re.split("=", case)
                if re.match("^x",splited[0]):
                    self.x = int(splited[1])
                elif re.match("^y",splited[0]):
                    self.y = int(splited[1])
