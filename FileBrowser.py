# -*- coding: utf-8 -*-
"""
Created on Fri Jan  8 09:36:02 2021

@author: seb
"""
from RleParser import RleParser
import os
import pickle
import re

class FileBrowser:
    
    def __init__(self, path):
        self.dictFiles = {}
        with os.scandir(path) as dirs:
            for entry in dirs:
                try:
                    print(entry.path)
                    name = re.split("\.",entry.name)[0]
                    rle = RleParser(entry.path)
                    self.dictFiles[name]=rle.matrix
                        
                except:
                    print("Error on "+entry.path)
        

