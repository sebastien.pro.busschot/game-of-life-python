# -*- coding: utf-8 -*-
"""
Created on Mon Jan 11 21:32:10 2021

@author: seb
"""

from FileBrowser import FileBrowser
import pickle

class FileManager:
    
    def __init__(self, path):
       self.getRle(path)
       
        
    def getRle(self, path):
        tried = False
        try:
            with open("dictRleFile", "rb") as file:
                self.dict = pickle.load(file)
                
        except Exception:
            if(tried == False):
                fileBrowser = FileBrowser(path)
                with open("dictRleFile", "wb") as file:
                    pickle.dump(fileBrowser.dictFiles ,file)
                    
                self.getRle(path)
                print("File created")
            else:
                print("Error")
                
    def printSheme(self, name):
        for line in self.dict[name]:
            print(line)
            
    def getSheme(self, name):
        return self.dict[name]
        
        
        
#debug
path = r"C:\Users\seb\Documents\dev\python\rle"
f = FileManager(path)